def calculator(a, b, operator):
    # ==============
    # Your code here
    sum = str(a) + operator + str(b)
    result = int(eval(sum))
    return int(bin(result)[2:])
    # ==============


print(calculator(2, 4, "+"))  # Should print 110 to the console
print(calculator(10, 3, "-"))  # Should print 111 to the console
print(calculator(4, 7, "*"))  # Should output 11100 to the console
print(calculator(100, 2, "/"))  # Should print 110010 to the console
