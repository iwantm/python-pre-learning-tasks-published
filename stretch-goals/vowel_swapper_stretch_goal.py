def replacer(list, a, b, B):
    count = -1
    for c in range(0, len(list)):
        if list[c].lower() == a:
            count += 1
            if count == 1:
                if list[c].isupper():
                    list[c] = B
                elif list[c].islower():
                    list[c] = b
                break

    return list


def vowel_swapper(string):
    # ==============
    # Your code here
    lst = list(string)
    lst = replacer(lst, "a",  '4', '4')
    string = replacer(lst, "o",  'ooo', '000')
    string = replacer(lst, "e",  '3', '3')
    string = replacer(lst, "i",  "!", '!')
    string = replacer(lst, "u",  "|_|", "|_|")
    replaced = ""
    replaced = replaced.join(lst)

    return replaced

    # ==============


# Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("aAa eEe iIi oOo uUu"))

# Should print "Hello Wooorld" to the console
print(vowel_swapper("Hello World"))

# Should print "Ev3rything's Av4!lable" to the console
print(vowel_swapper("Everything's Available"))
