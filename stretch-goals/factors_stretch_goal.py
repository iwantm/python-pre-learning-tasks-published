def factors(number):
    # ==============
    # Your code here
    factors = []
    for n in range(2, number):
        if number % n == 0:
            factors.append(n)
    if factors:
        return factors
    else:
        return str(number) + " is a prime number"
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
